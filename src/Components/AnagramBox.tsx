import React from 'react';
var equal = require('deep-equal');

const InputRow: React.FC<{}> = ({children}) => {
	return (<div
		style={{
			display:'flex',
			flexDirection:'row',
			justifyContent:'space-between',
			alignItems:'center',
			marginBottom:'10px'
		}}
	>
		{children}
	</div>);
}

type AnagramBoxState = {
	result: boolean | null;
}
type AnagramBoxProps = {
	onSubmit: (word1: string, word2: string) => void;
}
class AnagramBox extends React.Component<AnagramBoxProps, AnagramBoxState> {
	private firstRef = React.createRef<HTMLInputElement>();
	private secondRef = React.createRef<HTMLInputElement>();
	constructor(props: AnagramBoxProps) {
		super(props);
		this.state = {
			result: null
		}
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	// componentDidMount() {

	// }

	// componentDidUpdate() {

	// }

	shouldComponentUpdate(nextProps: AnagramBoxProps, nextState: AnagramBoxState) {
		return equal(nextState,this.state);
	}

	render() {
		return <React.Fragment>
			<form
				onSubmit={this.handleSubmit}
				style={{
					display:'flex',
					flexDirection:'column'
				}}
			>
				<InputRow>
					<label>First word:</label>
					<input
						type="text"
						name="first"
						ref={this.firstRef}
					/>
				</InputRow>
				<InputRow>
					<label>Second word:</label>
					<input
						type="text"
						name="second"
						ref={this.secondRef}
					/>
				</InputRow>
				<input type="submit" value="Submit" />
			</form>
			{this.state.result===false ? <div style={{color:'red',width:'100%',justifyContent:'center',alignItems:'center',padding:'20px'}}>This is not an anagram.</div> : 
				(this.state.result && <div style={{color:'green',width:'100%',justifyContent:'center',alignItems:'center',padding:'20px'}}>This is an anagram.</div>)}
		</React.Fragment>
	}

	handleSubmit(event: React.FormEvent<HTMLFormElement>) {
		event.preventDefault();

		// send example to db

		// check if this is anagram
		let firstInput = this.firstRef.current
		let secondInput = this.secondRef.current
		if (firstInput&&secondInput&&firstInput.value.length!==secondInput.value.length) {
			this.setState({...this.state,result:false},()=>{
				if (firstInput&&secondInput) this.props.onSubmit(firstInput.value, secondInput.value);
			})
		} else if (firstInput&&secondInput) {
			let result: boolean = true;
			let len: number = firstInput.value.length;
			for (let i: number=0;i<len;i++) {
				if (firstInput&&firstInput.value.charAt(i)!==secondInput.value.charAt(len-1-i)) {
					result = false;
					break;
				}
			}
			this.setState({result:result},()=>{
				if (firstInput&&secondInput) this.props.onSubmit(firstInput.value, secondInput.value);
			});
		}
	}
}

export default AnagramBox;