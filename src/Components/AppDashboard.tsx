import React from 'react';
import AnagramBox from './AnagramBox';
import MostPopularList from './MostPopularList';

const StackColumn: React.FC<{}> = ({children}) => {
	return (<div
		style={{
			display:'flex',
			flexDirection:'column',
			justifyContent:'flex-start',
			alignItems:'stretch'
		}}
	>
		{children}
	</div>);
}

type AppDashboardState = {
	items: string[] //{ name: string; }[]
}
class AppDashboard extends React.Component<{}, AppDashboardState> {
	constructor(props: any) {
		super(props);
		this.state = {
			items: []
		};
		this.refreshList = this.refreshList.bind(this);
	}

	componentDidMount() {
		this.fetchList();
	}

	// componentDidUpdate() {

	// }

	// shouldComponentUpdate() {

	// }



	render() {
		return <div
			style={{
				display:'flex',
				flexDirection:'column',
				justifyContent:'flex-start',
				alignItems:'center',
				padding:'10px',
			}}
		>
			<h1 style={{backgroundColor:'white',padding:'20px'}}>Anagram Check</h1>
			<div
				style={{
					display:'flex',
					flexDirection:'row',
					justifyContent:'space-between',
					alignItems:'stretch',
					padding:'20px',
					width:'500px',
					backgroundColor:'white'
				}}
			>
				<StackColumn>
					<h3>Input Form</h3>
					<AnagramBox onSubmit={this.refreshList} />
				</StackColumn>
				<StackColumn>
					<h3>10 Most Popular List</h3>
					<MostPopularList
						items={this.state.items}
						listStyle={{
							height:'500px'
						}}
					/> {/*make this into view */}
				</StackColumn>
			</div>
		</div>
	}

	fetchList() {
		fetch("http://localhost:9000/get", {
			method: 'GET',
			mode: 'cors',
			cache: 'no-cache',
			credentials: 'include',
			redirect: 'follow',
			referrerPolicy: 'origin',
			headers: {
				'X-PINGOVER': 'pingpong',
				'Origin': 'http://localhost:3000'
			}
		}).then(resp=>{
				return resp.json();
			}).then(json=>{
				if (json.error) {
					throw new Error(`server error: ${json.error}`);
				} else {
					console.log("data is "+JSON.stringify(json.data))
					this.setState({
						items: json.data
					});
				}
			}).catch(err=>{
				console.error(err.toString());
				alert("http error");
			});
	}
	refreshList(word1: string, word2: string) {
		let body: string = JSON.stringify({
				first: word1,
				second: word2
			});
		fetch("http://localhost:9000/record", {
			method: 'POST',
			mode: 'cors',
			cache: 'default',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json',
				'Origin': 'http://localhost:3000',
				'Accept': 'application/json',
				'Content-Length': body.length.toString(),
				'Access-Control-Request-Method': 'POST',
				'Access-Control-Request-Headers': 'Content-Type, Origin, Accept, Access-Control-Request-Headers, Access-Control-Request-Method'
			},
			redirect: 'follow',
			referrerPolicy: 'origin',
			body: body
		}).then(resp=>{
				return resp.json();
			}).then(json=>{
				if (json.error) {
					throw new Error(`server error: ${json.error}`);
				} else {
					console.log("data is "+JSON.stringify(json.data))
					this.setState({
						items: json.data
					});
				}
			}).catch(err=>{
				console.error(err.toString());
				alert("http error");
			});
	}
}

export default AppDashboard;