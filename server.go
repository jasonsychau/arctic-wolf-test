package main
import (
  "sort"
  "net/http"
  "encoding/json"
  "log"
  "strings"
)

type Request struct {
  First string `json:"first"`
  Second string `json:"second"`
}
type Response struct {
	Data []string `json:"data"`
}
type CountPair struct {
	Name string
	Count int
}
type ByCount []CountPair
func (a ByCount) Len() int { return len(a) }
func (a ByCount) Less(i, j int) bool { 
	if (a[i].Count == a[j].Count) {
		return a[i].Name > a[j].Name;
	} else { return a[i].Count < a[j].Count; }
}
func (a ByCount) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func handleGet(w http.ResponseWriter, r *http.Request) {
    method := strings.ToLower(r.Method)
	if method == "options" {
		setCORS(w)
	} else if method == "get" {
    	returnTopTen(countsStore, w)
	} else {
		http.Error(w, "Invalid action", http.StatusMethodNotAllowed)
	}
}
func handleRecord(w http.ResponseWriter, r *http.Request) {
	method := strings.ToLower(r.Method)
	if method == "options" {
		setCORS(w)
	} else if method == "post" {
	    var body Request
	    var firstWord string
	    var secondWord string
	    err := json.NewDecoder(r.Body).Decode(&body)
	    if err != nil {
	      setCORS(w)
	      http.Error(w, "Invalid JSON: " + err.Error(), http.StatusBadRequest)
	    } else {
	    	firstWord = body.First
	    	secondWord = body.Second
	    	var key string
	    	if (firstWord<secondWord) {
	    		key = firstWord + " & " + secondWord
	    	} else {
	    		key = secondWord + " & " + firstWord
	    	}

	    	countsStore[key] = countsStore[key] + 1

	      returnTopTen(countsStore, w)
	  }
	} else {
		http.Error(w, "Invalid action", http.StatusMethodNotAllowed)
	}
}
var countsStore = make(map[string]int)
func main (){
  http.HandleFunc("/get", handleGet)
  http.HandleFunc("/record", handleRecord)

  log.Fatal(http.ListenAndServe(":9000", nil))
}
func setCORS(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, HEAD, OPTIONS, PATCH")
	w.Header().Set("Access-Control-Max-Age","1000");
	w.Header().Set("Access-Control-Allow-Credentials", "true");
	w.Header().Set("Access-Control-Expose-Headers", "*");
	w.Header().Set("Access-Control-Allow-Headers","Content-Type, Authorization, X-Requested-With, Origin, Accept, Access-Control-Request-Headers, Access-Control-Request-Method, X-PINGOVER");
}
func returnTopTen(countsStore map[string]int, w http.ResponseWriter) {
	counts := make([]CountPair, 0, len(countsStore))
	for k := range countsStore {
		var nextPair CountPair
		nextPair.Name = k
		nextPair.Count = countsStore[k]
		counts = append(counts, nextPair)
	}

	sort.Sort(ByCount(counts))
	
	var size int
	var stop int
	if len(counts)<10 {
		size = len(counts)
		stop = 0
	} else {
		size = 10
		stop = len(counts) - 10
	}
	ret := make([]string, 0, size)
	for i := len(counts)-1; i >= stop; i-- {
		ret = append(ret, counts[i].Name)
	}

	var data = &Response{Data: ret}
	setCORS(w)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}